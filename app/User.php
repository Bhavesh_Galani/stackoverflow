<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * ACCESSORS
     */
    public function getAvatarAttribute() {
        $size = 40;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}";
    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * RELATIONSHIP METHODS
     */
    public function questions() {
        return $this->hasMany(Question::class);
    }
    public function answers() {
        return $this->hasMany(Answer::class);
    }
    public function favourites() {
        return $this->belongsToMany(Question::class)->withTimestamps();
    }
    public function votesQuestions()
    {
        return $this->morphedByMany(Question::class, 'vote')->withTimestamps();
    }
    public function votesAnswers()
    {
        return $this->morphedByMany(Answer::class, 'vote')->withTimestamps();
    }

    /**
     * HELPER FUNCTIONS
     */

    public function hasQuestionUpvote(Question $question)
    {
        return auth()->user()->votesQuestions()->where(['vote'=>1, 'vote_id'=>$question->id])->exists();
    }
    public function hasQuestionDownvote(Question $question)
    {
        return auth()->user()->votesQuestions()->where(['vote'=>-1, 'vote_id'=>$question->id])->exists();
    }
    public function hasVoteForQuestion(Question $question)
    {
        return $this->hasQuestionUpvote($question) || $this->hasQuestionDownvote($question);
    }
    
    public function hasAnswerUpvote(Answer $answer)
    {
        return auth()->user()->votesAnswers()->where(['vote'=>1, 'vote_id'=>$answer->id])->exists();
    }
    public function hasAnswerDownvote(Answer $answer)
    {
        return auth()->user()->votesAnswers()->where(['vote'=>-1, 'vote_id'=>$answer->id])->exists();
    }
    public function hasVoteForAnswer(Answer $answer)
    {
        return $this->hasAnswerUpvote($answer) || $this->hasAnswerDownvote($answer);
    }
}
